package no.experis;

public class PetRock {
    private String name;
    private boolean happy = false;

    public PetRock(String name){
        if(name.isEmpty())
            throw new IllegalArgumentException();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isHappy() {
        return this.happy;
    }

    public void playWithRock() {
        this.happy = true;
    }

    public String getHappyMessage(){
        if(!happy)
           throw new IllegalStateException();
        return "I'm Happy!";
    }

    public int getFaveNumber(){
        return 42; // OMG
    }

    public void waitForHappy(){
        while(!happy){
            // do nothing
        }
    }
}
