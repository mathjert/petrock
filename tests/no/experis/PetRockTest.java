package no.experis;

import jdk.nashorn.internal.ir.annotations.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    void setUp() {
        rocky = new PetRock("Rocky");
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    void TestUnhappyToStart() throws Exception {
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() throws Exception {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    //@Disabled("Such ignore bcse not implement Except")
    @Test
    void nameFail() throws Exception {
       assertThrows(IllegalStateException.class, () -> rocky.getHappyMessage());
    }

    @Test
    void name() throws Exception {
        rocky.playWithRock();
        assertEquals("I'm Happy!", rocky.getHappyMessage());
    }

    @Test
    void testFaveNum() throws Exception {
        assertEquals(42, rocky.getFaveNumber());
    }

    @Test
    void emptyNameFail() throws Exception {
        assertThrows(IllegalArgumentException.class, () -> new PetRock(""));
    }

    @Test
    void waitForHappyTimeOut() throws Exception {
       // assertTimeout(Duration.ofMillis(1000), () -> rocky.waitForHappy());
        assertTimeoutPreemptively(Duration.ofMillis(1000), () -> rocky.waitForHappy());
    }
}